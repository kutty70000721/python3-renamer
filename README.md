# RENAMER

##  NAME
Python utility for selective deleting symbols in names of files and directories.

## INSTALLATION

1. Go to the directory with renamer
2. cp ./renamer.py /usr/bin/renamer
3. chmod +x /usr/bin/renamer 
4. chown root:root /usr/bin/renamer 
5. Install modules in requirements.txt with pip

## SYNOPSIS

    renamer [OPTION]... [DIRECTORY]... 


## DESCRIPTION

Change names recursively:

    -r, -R, --recursively 

Make log 'renamer_log.txt' in your $HOME directory. The format of a log: "old file name ===> new file name":

    -log, -l

Words for deletion:

    -w, --words WORD

## Example:
Delete phrase 'XYZ' in names in /path/to/directory recursively.
Command:

    'renamer --words 'XYZ' -r path/to/directory'

Before:

    /directory
         fileXYZ
         another_directory/
             another_file
             XYZ_another_file
             one_more_XYZ_directory/

After:

    /directory
         file
         another_directory/
             another_file
             _another_file
             1one_more__directory/

